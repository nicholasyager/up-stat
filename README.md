# 2015 UP-Stat Data Competition
#### Three Degrees of Freedom: Nicholas A. Yager, Matthew Taylor, and Thomas Hartvigsen

## Success:
Our traffic analysis was a finalist at the [2015 UP-Stat][UPSTAT] [data competition][datacomp] in Geneseo, NY. The complete report can be found [here][finalreport].

## Progress:

- 2015-02-20: First report published on [exploratory analysis][report1]
- 2015-02-25: Second report published on [root-mean-squared deviation][report2]
- 2015-03-09: Added pairwise RMSD calculations to pick out any abnormal days.
- 2015-03-16: Added correlation analysis. The results are very similar to the
  RMSD data, but slightly more understandable.
- 2015-03-18: Added some basic trend analysis.
- 2015-03-20: Added a CLARA clustering analysis to detect major patterns in traffic.
- 2015-03-20: Submitted!

## References
City of Rochester, _[Property Information Application][zoning]_

City of Rochester, _[Traffic Volume Map][traffic_map]_. 2014

Flynn MR, Kasimov AR, Nave J, Rosales RR, Seibold B. [Traffic Modeling - Phandom Traffic Jams and Traveling Jamitons][Flynn]

Piccoli B, Tosin A. [A Review of Continuum Mathematical Models of Vehicular Traffic.][piccoli] 
    _Encyclopedia of Complexity and Systems Science_. 2009. pp 9729-9749.
    
Transportation REsearch Board of th NAtional Academies, _Highway Capacity Manual_. 5th Edition. 2010.

Goatin P. [The Aw-Rascle traffic flow model with phase transitions.][goatin]. 2005.

<!-- Links! -->
[report1]: http://intranet.nicholasyager.com/traffic/exploratory_analysis.html
[report2]:http://intranet.nicholasyager.com/traffic/rmsd_pattern_analysis.html
[Flynn]:http://math.mit.edu/projects/traffic/
[piccoli]: http://www.iac.rm.cnr.it/~piccoli/PapersFiles/PbTa-review_traffic-SPRINGER%5B1%5D.pdf
[goatin]:http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=AD6D5F5E53A0D74CB878B4181C808625?doi=10.1.1.102.7931&rep=rep1&type=pdf
[zoning]:http://maps.cityyofrochester.gov/propinfo/
[traffic_map]: http://www2.monroecounty.gov/files/dot/pdfs/City-adt-map-through-2014.pdf
[finalreport]: https://github.com/nicholasyager/up-stat/raw/master/report/nonanon/upstat_report_anon.pdf
[UPSTAT]:http://up-stat.org
[datacomp]:http://www.up-stat.org/DataCompetition/Scoreboard
